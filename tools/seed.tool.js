const Casual = require('casual');
const DataModel = require('./schemas/record.schema');

// grab the CLI args.
const args = process.argv;
const argValues = {
  numberOfRecords: 4,
};

args.forEach((argString) => {
  const splitString = argString.split('=');
  if (splitString.length !== 2) { return; }
  if (argValues[splitString[0]]) {
    console.log('set value', splitString[0], 'with', splitString[1]);
    argValues[splitString[0]] = splitString[1];
  }
});

const writeArray = [];
// Generate an array of random data to write to the DB
for (let i = 0; i <= parseInt(argValues.numberOfRecords, 10); i += 1) {
  const newObject = {
    author: Casual.full_name,
    content: Casual.sentence,
    title: Casual.catch_phrase,
    id: i,
  };
  writeArray.push(newObject);
  console.log('Generated record', newObject);
}
DataModel.create(writeArray)
  .then((records) => {
    console.log(records);
  }).error((err) => {
    console.error(err);
  });

process.exit();
