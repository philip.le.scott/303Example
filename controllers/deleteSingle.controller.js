const Logger = require('../helpers/logger.helper')(__filename);
const RecordSchema = require('../schemas/record.schema.js');
/**
 * This controller is responsible for deleting a single record.
 * @module controllers/deleteSingle
 */
module.exports = async function deleteSingleController(req, res) {
  // make sure we were given a value for the id to delete.
  if (!req.params.id) {
    Logger.error('Invalid data for id provided for delete');
    return res.status(400).json({
      message: 'Invalid or bad data was provided',
      success: false,
    });
  }

  // we can't use a string that doesn't convert to a number for look ups.
  let id;
  try {
    id = parseInt(req.params.id, 10);
  } catch (err) {
    Logger.error(
      `Invalid deleteion id provided. Found value ${req.params.id} but expected to be castable to Number`,
      err,
    );
    return res.status(500).json({ message: err, success: false });
  }

  // Do the actual look up and return the single record when the look up resolves.
  return RecordSchema
    .deleteOne({ id })
    .then((record) => {
      Logger.debug('Successfully removed record with id', req.params.id);
      res.status(200).json({ payload: record, success: true });
    })
    .catch((err) => {
      Logger.error('There was an error deleting a record', err);
      res.status(500).json({ message: err, success: false });
    });
};
