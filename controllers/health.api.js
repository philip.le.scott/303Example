const Logger = require('../helpers/logger.helper')(__filename);
/**
 * This controller is responsible for signalling the service is stil alive.
 * @module controllers/healthCheck
 */
module.exports = async function healthCheckController(req, res) {
  Logger.info('Health ping');
  return res.status(200).send();
};
