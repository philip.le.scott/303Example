const Logger = require('../helpers/logger.helper')(__filename);
const RecordModel = require('../schemas/record.schema.js');
/**
 * This controller is responsible for creating a single record.
 * @module controllers/postSingle
 */
module.exports = async function postSingleController(req, res) {
  // Load the sent data payload into a model to be validated.
  const dataModel = new RecordModel(req.body);
  /*
  * run a sync validation because if the data is bad we don't want to continue
  * processing.
  */
  const validationErrors = dataModel.validateSync();

  // check for validation issues on the data and return an error if there is any.
  if (validationErrors) {
    Logger.error('Invalid data for POST request', validationErrors);
    res.status(400).json({
      message: 'Invalid or bad data was provided',
      success: false,
    });

  // Used an else here due to the AirBnB code style and consistent returns rule.
  } else { // if validation is good then push the record to Mongo
    dataModel
      .save()
      .then((record) => {
        Logger.info('Successful record creation');
        res.status(200).json({ payload: record, success: true });
      })
      .catch((err) => {
        Logger.error('There was an error saving a new record', err);
        res.status(500).json({ message: err, success: false });
      });
  }
};
