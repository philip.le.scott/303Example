const Mongoose = require('mongoose');
const MongoHelper = require('../helpers/mongo.helper');

/**
 * A small mongoose schema that defines our API contract.
 * @module schemas/Record.
 */
/*
* define our data fields that we will store in mongo here. We will also use this
* as a method of validation for APIs.
*/
const schema = new Mongoose.Schema({
  title: { type: String, required: true },
  id: { type: Number, index: true },
  author: { type: String, required: true },
  content: { type: String, required: true },
});
schema.plugin(MongoHelper.autoIncrement, { inc_field: 'id' });

module.exports = Mongoose.model('record', schema);
