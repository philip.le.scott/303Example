/**
* A simple quality of life helper to create new Bunyan loggers.
* You can normally just pass in the executing file name as the instance name.
* This makes it easy ans simple to track the stack trace.
*
* For full details and doc please see : https://www.npmjs.com/package/bunyan
* @module BuynanLogger
* @example
*   const Logger = require('../helpers/logger.helper')(__filename);
*   Logger.info('I am a logger!');
*   Logger.error('Oh no! An error took place.');
*
*   //Uses `util.format` for msg formatting.
*   Logger.info('hi %s', bob, anotherVar);
*
*   // Adds "foo" field to log record. You can add any number of additional fields here.
*   Logger.info({foo: 'bar'}, 'hi');
*/
const Bunyan = require('bunyan');
// https://www.npmjs.com/package/bunyan-loggly
const BunyanLoggly = require('bunyan-loggly');
const constants = require('./constants.helper');

const streams = [
  {
    level: 'info',
    stream: process.stdout,
  },
  {
    level: 'warn',
    stream: process.stdout,
  },
  {
    level: 'error',
    stream: process.stdout,
  },
];
/**
 * Creates a new logger instance when called with a name. It will also start
 * streaming to loggly if the env found is not a Dev one.
 * @param  {String} name The module name that is creating the logger.
 * @return {Object}      Bunyan.Logger
 */
function createBunyanInstance(name) {
  // if we are in dev mode, don't stream the logs to the loggly service.
  if (constants.ENV === 'dev') {
    return Bunyan.createLogger({
      name,
      streams,
    });
  }
  if (constants.LOGGLY.SUBDOMAIN && constants.LOGGLY.TOKEN && constants.ENV !== 'dev') {
    /* To add buffering/batching of logs add a second and third param to the the
       constructor here. i.e 5, 500 . The first param is batch number, the second
       is the timeout threshold in ms.
       then the stream will send a batch of logs every 5 new log entries. */
    const logglyStream = new BunyanLoggly({
      token: constants.LOGGLY.TOKEN,
      subdomain: constants.LOGGLY.SUBDOMAIN,
    });
    streams.push({
      type: 'raw',
      stream: logglyStream,
    });
  }

  return Bunyan.createLogger({
    name,
    streams,
  });
}
module.exports = createBunyanInstance;
